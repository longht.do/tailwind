import './App.css';
import React, { useState, useEffect } from 'react';
import Footer from './components/Footer';
import Navbar from './components/Navbar';
import Home from './pages/index';
import About from './pages/about';
import Menu from './pages/menu';
import { Switch, Route } from 'react-router-dom';
import Dropdown from './components/Dropdown';

function App() {
  const [openDropdown, setOpenDropdown] = useState(false);

  const handleClick = () => {
    setOpenDropdown(!openDropdown);
  }

  useEffect(() => {
    const hideMenu = () => {
      if (window.innerWidth > 768 && openDropdown) {
        setOpenDropdown(false);
        console.log('it works')
      }
    }
    window.addEventListener('resize', hideMenu);

    return () => {
      window.addEventListener('resize', hideMenu);
    }
  })

  return (
    <>
      <Navbar handleClick={handleClick}/>
        <Dropdown openDropdown={openDropdown} handleClick={handleClick} />
        <Switch>
          <Route path='/' exact component={Home} />
          <Route path='/about' component={About} />
          <Route path='/menu' component={Menu} />
        </Switch>
      <Footer />
    </>
  );
}

export default App;
