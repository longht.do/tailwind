import React from 'react';
import { Link } from 'react-router-dom';

const Dropdown = ({ openDropdown, handleClick }) => {
  return (
    <div className={openDropdown ? 'justify-center items-center bg-yellow-500 grid grid-rows-4' : 'hidden'} onClick={handleClick}>
      <Link to='/' className='p-4'>Home</Link>
        <Link to='/Menu' className='p-4'>Menu</Link>
        <Link to='/About' className='p-4'>About</Link>
        <Link to='/Contact' className='p-4'>Contact</Link>
    </div>
  )
}

export default Dropdown
