import React from 'react';
import PictureOne from '../images/HanhAndLong-77.jpg';
import PictureTwo from '../images/HanhAndLong-82.jpg';



const Content = () => {
  return (
    <div>
      <div className="flex flex-col justify-content items-center bg-white h-screen font-mono py-40">
        <img src={PictureOne} alt="ok" className="h-full rounder mb-20 shadow"/>
        <div className="flex flex-col justify-content items-center">
          <h2 className="text-2xl mb-2">Long & Canh</h2>
          <p className="mb-2">Looking good you 2</p>
        </div>
      </div>
      <div className="flex flex-col justify-content items-center bg-white h-screen font-mono py-40">
        <img src={PictureTwo} alt="very-cool" className="h-full rounder mb-20 shadow"/>
        <div className="flex flex-col justify-content items-center">
          <h2 className="text-2xl mb-2">Long & Kevin</h2>
          <p className="mb-2">Looking even better</p>
        </div>
      </div>
  </div>
  )
}

export default Content
