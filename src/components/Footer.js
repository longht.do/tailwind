import React from 'react'

function Footer() {
  return (
    <div>
      <div className="flex justify-content items-center h-16 bg-black text-white ">
        <p>Copyright by longdodo</p>
      </div>
    </div>
  )
}

export default Footer
